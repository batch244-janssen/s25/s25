// console.log("Hello World!")

// [SECTION] JSON 
/*
	- JavaScript Object Notation - is a data format used by applications to store and transport data to one another.
	- Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript objects
	- JavaScript objects are not to be confused with JSON
	- JSON is used for serializing different data types into bytes
	- Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
	- Uses double quotes for property names
	Syntax:
	{
		"propertyA": "valueA",
		"propertyB": "valueB"
	}
*/

// [SECTION] JSON Objects
	// {
	// 	"city" : "Quezon City",
	// 	"province" : "Metro Manila",
	// 	"country": "Philippines"
	// }

// [SECTION] JSON Arrays

// "cities": [
// 	{ "city": "Quezon City", "province": "Metro Manila","country": "Philippines"},
// 	{ "city": "Manila", "province": "Metro Manila","country": "Philippines"},
// 	{ "city": "Makati", "province": "Metro Manila","country": "Philippines"}
// ]

// "singers": [
// 	{
// 		"name": "Jason Mraz",
// 		"song": "I'm Yours", 
// 		"gender": "male"
// 	},
// 	{
// 		"name": "Jason Derulo",
// 		"song": "Trumpets", 
// 		"gender": "male"
// 	},
// 	{
// 		"name": "Ara Mina",
// 		"song": "Aray", 
// 		"gender": "female"
// 	}
// ]

// [SECTION] JSON Methods

// Converting Data into Stringified JSON
	/*
		- Stringified JSON is a JavaScript object converted into a string to be used in other functions of a JavaScript application
		- They are commonly used in HTTP requests where information is required to be sent and received in a stringified JSON format
		- Requests are an important part of programming where an application communicates with a backend application to perform different tasks such as getting/creating data in a database
	*/
let batches = [
	{
		batchName: "Batch 244",
		description: "Part-time"
	},
	{
		batchName: "Batch 243",
		description: "Full-time"
	}
];
console.log(batches);

// "stringify" method is used to convert JabaScript objects into a string
console.log(`Result from stringify method:`);
console.log(JSON.stringify(batches));

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});
console.log(data);

// Using Stringify Method with Variables

/*
	- When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
	- The "property" name and "value" would have the same name which can be confusing for beginners
	- This is done on purpose for code readability meaning when an information is stored in a variable and when the object created to be stringified is created, we supply the variable name instead of a hard coded value
	- This is commonly used when the information to be stored and sent to a backend application will come from a frontend application
	- Syntax
		JSON.stringify({
			propertyA: variableA,
			propertyB: variableB
		});
	- Since we do not have a frontend application yet, we will use the prompt method in order to gather user data to be supplied to the user details
*/

// let firstName = prompt(`What is your first name?`);
// let lastName = prompt("What is your last name?");
// let age = prompt("How old are you?");
// let address = {
// 	city: prompt("Which city do you live in?"),
// 	country: prompt("Which country does your city belong to?")
// };

// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// });
// console.log(otherData);

// [SECTION] Converting stringified JSON into JS Objects

	/*
		- Objects are common data types used in applications because of the complex data structures that can be created out of them
		- Information is commonly sent to applications in stringified JSON and then converted back into objects
		- This happens both for sending information to a backend application and sending information back to a frontend application
	*/
let batchesJSON = `[
	{"batchName": "Batch 244"}, 
	{"batchName": "Batch 243"}
]`
console.log(batchesJSON);

console.log(`Result from parse method:`);
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name": "John",
	"age" : "31",
	"address": {
		"city": "Manila",
		"country": "Philippines"
	}
}`
console.log(JSON.parse(stringifiedObject));

let stringifiedObject1 = JSON.parse(`{
	"name": "John",
	"age" : "31",
	"address": {
		"city": "Manila",
		"country": "Philippines"
	}
}`);
console.log(stringifiedObject1)
